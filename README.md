﻿# NCKU-sCourserer-dbUpdater
***NCKU-sCourserer is a project aiming to bring a better course selecting experience. I hope that you won't need to open 50 browser tabs just to find a decent course fitting in your current course schedule anymore.***

This repo is responsible for course info viewing and filtering. 
Based on https://github.com/greyli/bootstrap-flask
## Features(most are WIP)
- multi-department selection
- nckuhub integration
- course schedule importing/planning
- auto course selecting
## How to run this program
**Install these dependencies first**
- Python 3.9.x or newer
- After installing Python, run the following command while under a virtualenv to install required Python packages
```sh
pip install -r requirements.txt
```
**Afterwards, run the following command in the same virtualenv**
```
flask run
```
**Finally, enter the following URL in your browser**
```
localhost:5000
```

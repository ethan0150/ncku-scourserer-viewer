from sqlalchemy import Column, Integer, String, ForeignKey, JSON
from base import Base
from dataclasses import dataclass
# from typing import List


@dataclass
class Dept(Base):
    __tablename__ = 'dept'

    id: str
    name: str
    abber: str

    id = Column(String, primary_key=True)
    name = Column(String)
    abber = Column(String)


@dataclass
class College(Base):
    __tablename__ = 'college'

    id: str
    depts: JSON

    id = Column(String, primary_key=True)
    depts = Column(JSON)


'''
if __name__ == '__main__':
    engine = create_engine("sqlite+pysqlite:///db.sqlite", echo=True, future=True)
    Base.metadata.create_all(engine)
    Session = sessionmaker(engine)
    with Session() as s:
        s.add(Dept(id='A1', name='外語中心', aber='FLC'))
        s.commit()
'''

# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, flash, Markup, jsonify
import requests
from flask_wtf import FlaskForm, CSRFProtect
from wtforms import StringField, SubmitField, BooleanField, PasswordField, IntegerField, TextField,\
    FormField, SelectField, FieldList
from wtforms.validators import DataRequired, Length
from wtforms.fields import *
# from sqlalchemy.orm import sessionmaker
# from sqlalchemy import create_engine
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
import os
import json
app = Flask(__name__)
app.secret_key = 'dev'
DBurl = 'https://gitlab.com/ethan0150/ncku-scourserer-dbupdater/-/jobs/artifacts/main/raw/db.sqlite?job=test'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db.sqlite')

# set default button sytle and size, will be overwritten by macro parameters
app.config['BOOTSTRAP_BTN_STYLE'] = 'primary'
app.config['BOOTSTRAP_BTN_SIZE'] = 'sm'

# set default icon title of table actions
app.config['BOOTSTRAP_TABLE_VIEW_TITLE'] = 'Read'
app.config['BOOTSTRAP_TABLE_EDIT_TITLE'] = 'Update'
app.config['BOOTSTRAP_TABLE_DELETE_TITLE'] = 'Remove'
app.config['BOOTSTRAP_TABLE_NEW_TITLE'] = 'Create'
app.config['BOOTSTRAP_BOOTSWATCH_THEME'] = 'darkly'

bootstrap = Bootstrap(app)
db = SQLAlchemy(app)
csrf = CSRFProtect(app)
from relation import *
'''
print('Updating database')
with open('./db.sqlite', 'wb') as f:
    f.write(requests.get(DBurl, allow_redirects=True).content)
'''
# sess = None

with open('./db.sqlite', 'wb') as f:
    f.write(requests.get(DBurl, allow_redirects=True).content)

class ExampleForm(FlaskForm):
    """An example form that contains all the supported bootstrap style form fields."""
    date = DateField(description="We'll never share your email with anyone else.")  # add help text with `description`
    datetime = DateTimeField(render_kw={'placeholder': 'this is placeholder'})  # add HTML attribute with `render_kw`
    image = FileField(render_kw={'class': 'my-class'})  # add your class
    option = RadioField(choices=[('dog', 'Dog'), ('cat', 'Cat'), ('bird', 'Bird'), ('alien', 'Alien')])
    select = SelectField(choices=[('dog', 'Dog'), ('cat', 'Cat'), ('bird', 'Bird'), ('alien', 'Alien')])
    selectmulti = SelectMultipleField(choices=[('dog', 'Dog'), ('cat', 'Cat'), ('bird', 'Bird'), ('alien', 'Alien')])
    bio = TextAreaField()
    title = StringField()
    secret = PasswordField()
    remember = BooleanField('Remember me')
    submit = SubmitField()


class HelloForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(1, 20)])
    password = PasswordField('Password', validators=[DataRequired(), Length(8, 150)])
    remember = BooleanField('Remember me')
    submit = SubmitField()


class ButtonForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(1, 20)])
    submit = SubmitField()
    delete = SubmitField()
    cancel = SubmitField()


class TelephoneForm(FlaskForm):
    country_code = IntegerField('Country Code')
    area_code = IntegerField('Area Code/Exchange')
    number = TextField('Number')


class IMForm(FlaskForm):
    protocol = SelectField(choices=[('aim', 'AIM'), ('msn', 'MSN')])
    username = TextField()


class ContactForm(FlaskForm):
    first_name = TextField()
    last_name = TextField()
    mobile_phone = FormField(TelephoneForm, label='')
    office_phone = FormField(TelephoneForm)
    emails = FieldList(TextField("Email"), min_entries=3)
    im_accounts = FieldList(FormField(IMForm), min_entries=2)


'''
class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text, nullable=False)
    author = db.Column(db.String(100), nullable=False)
    category = db.Column(db.String(100), nullable=False)
    draft = db.Column(db.Boolean, default=False, nullable=False)
    create_time = db.Column(db.Integer, nullable=False, unique=True)
'''


class LeftColForm(FlaskForm):
    day_of_week = SelectField('星期', default=u'',
        choices=[('-1', u''), ('0', u'星期一'), ('1', u'星期二'), ('2', u'星期三'), ('3', u'星期四'), ('4', u'星期五'), ('5', u'星期六'), ('6', u'星期日')])
    course_name = StringField('課程名稱')
    lecturer_name = StringField('教師姓名')
    # subBtn = SubmitField('查詢')


class RightColForm(FlaskForm):
    collList = College.query.all()
    selList = list(zip([i for i in range(len(collList))], [j.id for j in collList]))
    selList.insert(0, ('-1', u''))
    collSel = SelectField('學院', default=u'', choices=selList)
    deptSel = SelectMultipleField('系所', choices=[], render_kw={'size': 6})


class FilterForm(FlaskForm):
    TLIST = ['0', '1', '2', '3', '4', 'N', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E']
    section = SelectMultipleField('節次', choices=[(t, t) for t in TLIST], render_kw={'size': 10})
    left_col = FormField(LeftColForm)
    right_col = FormField(RightColForm)

    '''
    collList = College.query.all()
    selList = list(zip([i for i in range(len(collList))], [j.id for j in collList]))
    selList.insert(0, ('-1', u''))
    coll_sel = SelectField('學院', default=u'', choices=selList)
    '''

'''
@app.before_first_request
def before_first_request_func():
'''


@app.route('/', methods=['GET', 'POST'])
def index():
    form = FilterForm()
    return render_template('index.html', form=form)


@app.route('/form', methods=['GET', 'POST'])
def test_form():
    form = HelloForm()
    return render_template('form.html', form=form, telephone_form=TelephoneForm(), contact_form=ContactForm(), im_form=IMForm(), button_form=ButtonForm(), example_form=ExampleForm())


@app.route('/nav', methods=['GET', 'POST'])
def test_nav():
    return render_template('nav.html')


@app.route('/pagination', methods=['GET', 'POST'])
def test_pagination():
    page = request.args.get('page', 1, type=int)
    pagination = Message.query.paginate(page, per_page=10)
    messages = pagination.items
    return render_template('pagination.html', pagination=pagination, messages=messages)


@app.route('/flash', methods=['GET', 'POST'])
def test_flash():
    flash('A simple default alert—check it out!')
    flash('A simple primary alert—check it out!', 'primary')
    flash('A simple secondary alert—check it out!', 'secondary')
    flash('A simple success alert—check it out!', 'success')
    flash('A simple danger alert—check it out!', 'danger')
    flash('A simple warning alert—check it out!', 'warning')
    flash('A simple info alert—check it out!', 'info')
    flash('A simple light alert—check it out!', 'light')
    flash('A simple dark alert—check it out!', 'dark')
    flash(Markup('A simple success alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.'), 'success')
    return render_template('flash.html')


'''
@app.route('/table')
def test_table():
    page = request.args.get('page', 1, type=int)
    pagination = Message.query.paginate(page, per_page=10)
    messages = pagination.items
    titles = [('id', '#'), ('text', 'Message'), ('author', 'Author'), ('category', 'Category'), ('draft', 'Draft'), ('create_time', 'Create Time')]
    return render_template('table.html', messages=messages, titles=titles)


@app.route('/table/<message_id>/view')
def view_message(message_id):
    message = Message.query.get(message_id)
    if message:
        return f'Viewing {message_id} with text "{message.text}". Return to <a href="/table">table</a>.'
    return f'Could not view message {message_id} as it does not exist. Return to <a href="/table">table</a>.'


@app.route('/table/<message_id>/edit')
def edit_message(message_id):
    message = Message.query.get(message_id)
    if message:
        message.draft = not message.draft
        db.session.commit()
        return f'Message {message_id} has been editted by toggling draft status. Return to <a href="/table">table</a>.'
    return f'Message {message_id} did not exist and could therefore not be edited. Return to <a href="/table">table</a>.'


@app.route('/table/<message_id>/delete', methods=['POST'])
def delete_message(message_id):
    message = Message.query.get(message_id)
    if message:
        db.session.delete(message)
        db.session.commit()
        return f'Message {message_id} has been deleted. Return to <a href="/table">table</a>.'
    return f'Message {message_id} did not exist and could therefore not be deleted. Return to <a href="/table">table</a>.'


@app.route('/table/new-message')
def new_message():
    return 'Here is the new message page. Return to <a href="/table">table</a>.'
'''


@app.route('/icon')
def test_icon():
    return render_template('icon.html')


@app.route('/collQry')
def collRes():
    '''
    qry = College.query.get(coll).depts
    print('qry:' + qry)
    print(type(qry))
    return qry
    '''
    joinedDict = {}
    collList = College.query.all()
    for coll in collList:
        joinedDict[coll.id] = []
        # print(json.loads(coll.depts))
        for dept in json.loads(coll.depts):
            qry = Dept.query.get(dept)
            # print(qry)
            joinedDict[coll.id].append([qry.id, qry.name, qry.abber])

    # print(joinedDict)
    return jsonify(joinedDict)


if __name__ == '__main__':
    '''
    print("fuck")
    with open('./db.sqlite', 'wb') as f:
        f.write(requests.get(DBurl, allow_redirects=True).content)
    engine = create_engine(
        "sqlite+pysqlite:///db.sqlite", echo=True, future=True)
    Session = sessionmaker(engine)
    global sess
    sess = Session()
    '''
    app.run(debug=True, use_reloader=False)
    '''
    print('bruh')
    sess.close()
    '''
